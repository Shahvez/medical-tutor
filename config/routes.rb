Rails.application.routes.draw do

  root :to => "home#index"

  # devise_for :users
  devise_for :users, controllers: {
      sessions: 'user/sessions'#,
      # registrations: 'user/registrations',
      # confirmations: 'user/confirmations',
      # passwords: 'user/passwords',
  }
  
  devise_scope :user do
    match "/director_sign_in" => "user/sessions#director_sign_in", via: [:get, :post]
 end
  
  resources :home
  
  namespace :admin do
    resources :homes
    resources :directors
    resources :counsellors
    resources :faculties
    resources :students
    resources :courses
    resources :course_units
    resources :course_sub_units

    get "/course_units_by_course" => "course_units#course_units_by_course"
  end

  namespace :director do
    resources :homes
    resources :counsellors
    resources :faculties
    resources :students
  end
end
