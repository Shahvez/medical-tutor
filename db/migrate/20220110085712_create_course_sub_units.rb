class CreateCourseSubUnits < ActiveRecord::Migration[6.1]
  def change
    create_table :course_sub_units do |t|
      t.integer :course_unit_id
      t.string :title
      t.text :description
      t.boolean :is_active,                       default: false
      t.integer :created_by
      t.integer :modified_by
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
