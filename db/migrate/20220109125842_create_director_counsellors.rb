class CreateDirectorCounsellors < ActiveRecord::Migration[6.1]
  def change
    create_table :director_counsellors do |t|
      t.integer :director_id
      t.integer :counsellor_id

      t.timestamps
    end
  end
end
