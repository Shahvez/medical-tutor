class CreateCourses < ActiveRecord::Migration[6.1]
  def change
    create_table :courses do |t|
      t.string :uuid,               null: false,  default: ""
      t.string :title
      t.text :description
      t.boolean :is_active,                       default: false
      t.integer :created_by
      t.integer :modified_by
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
