require "test_helper"

class Admin::CounsellorsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_counsellors_index_url
    assert_response :success
  end
end
