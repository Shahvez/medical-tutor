require "test_helper"

class Admin::CourseSubUnitsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_course_sub_units_index_url
    assert_response :success
  end

  test "should get new" do
    get admin_course_sub_units_new_url
    assert_response :success
  end

  test "should get edit" do
    get admin_course_sub_units_edit_url
    assert_response :success
  end
end
