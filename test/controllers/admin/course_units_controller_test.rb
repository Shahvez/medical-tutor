require "test_helper"

class Admin::CourseUnitsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_course_units_index_url
    assert_response :success
  end

  test "should get new" do
    get admin_course_units_new_url
    assert_response :success
  end

  test "should get edit" do
    get admin_course_units_edit_url
    assert_response :success
  end
end
