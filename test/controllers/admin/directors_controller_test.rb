require "test_helper"

class Admin::DirectorsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_directors_index_url
    assert_response :success
  end
end
