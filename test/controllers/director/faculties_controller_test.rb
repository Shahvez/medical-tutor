require "test_helper"

class Director::FacultiesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get director_faculties_index_url
    assert_response :success
  end

  test "should get new" do
    get director_faculties_new_url
    assert_response :success
  end

  test "should get edit" do
    get director_faculties_edit_url
    assert_response :success
  end
end
