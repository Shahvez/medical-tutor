require "test_helper"

class Director::StudentsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get director_students_index_url
    assert_response :success
  end

  test "should get new" do
    get director_students_new_url
    assert_response :success
  end

  test "should get edit" do
    get director_students_edit_url
    assert_response :success
  end
end
