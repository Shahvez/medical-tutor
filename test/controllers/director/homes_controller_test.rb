require "test_helper"

class Director::HomesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get director_homes_index_url
    assert_response :success
  end

  test "should get new" do
    get director_homes_new_url
    assert_response :success
  end

  test "should get edit" do
    get director_homes_edit_url
    assert_response :success
  end
end
