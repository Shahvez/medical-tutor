require "test_helper"

class Director::CounsellorsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get director_counsellors_index_url
    assert_response :success
  end

  test "should get new" do
    get director_counsellors_new_url
    assert_response :success
  end

  test "should get edit" do
    get director_counsellors_edit_url
    assert_response :success
  end
end
