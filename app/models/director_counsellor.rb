class DirectorCounsellor < ApplicationRecord
    belongs_to :director
    belongs_to :counsellor
end
