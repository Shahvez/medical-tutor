class Counsellor < User

    #### Relations ####
    has_many :director_counsellors
    has_many :directors, through: :director_counsellors

    ### Scope ####
    default_scope { where(:user_type => 'counsellor') }

    #### Callbacks ####
    # after_save :add_director_counsellor
    before_save :set_status
    before_update :update_status
    # after_touch :set_status


    private

    def self.add_director_counsellor(director:, counsellor:)
        director_counsellor = DirectorCounsellor.new(director_id: director, counsellor_id: counsellor)
        director_counsellor.save
    end

    def set_status
        self.status = 'rejected'
    end

    def update_status
        self.status = 'approved'
    end

end
