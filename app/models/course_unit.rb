class CourseUnit < ApplicationRecord
    #### Acts as Paranoid ####
    acts_as_paranoid

    #### Relations ####
    belongs_to :course
    has_many :course_sub_units, dependent: :destroy

end
