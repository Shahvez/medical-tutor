class CourseSubUnit < ApplicationRecord
    #### Acts as Paranoid ####
    acts_as_paranoid

    #### Relations ####
    belongs_to :course_unit

end
