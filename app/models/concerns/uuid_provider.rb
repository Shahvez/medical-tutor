module UuidProvider

  extend ActiveSupport::Concern

  included do
    before_validation :provide_uuid, on: :create
  end

  private

  def provide_uuid
    self.uuid ||= SecureRandom.uuid
  end
  
end