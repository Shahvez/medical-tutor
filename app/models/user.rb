class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  #### Acts as Paranoid ####
  acts_as_paranoid

  #### Concerns ####
  include UuidProvider

  #### Uploaders ####
  mount_uploader :image, ImageUploader

  #### Enums ####
  enum user_type: %w(admin director counsellor faculty student)
  enum status: %w(approved rejected)
  enum gender: %w(male female other)
  # enum is_active: %w(inactive active)

  #### Relations ####
  has_many :user_images
  # has_many :counsellors, through: :director_counsellors

  ### Scope ####
  # default_scope { where(:is_active => true) }

  #### Callbacks ####
  before_save :update_user_details

  GENDER = {
    0 => "male",
    1 => "female",
    2 => "other"
  }

  USER_TYPE = {
    0 => "admin",
    1 => "director",
    2 => "counsellor",
    3 => "faculty",
    4 => "student"
  }
  
  STATUS = {
    0 => "approved",
    1 => "rejected"
  }
  def update_user_details
    self.user_type = 4 if !self.user_type.present?
    self.status = 'approved' if !self.status.present?
    # self.latitude = ''
    # self.longitude = ''
    # self.timezone = ''
  end

  def address
    if self.zip
      "#{self.street_address}, #{self.city}, #{self.state}, #{self.zip}"
    end
  end
end
