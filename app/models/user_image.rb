class UserImage < ApplicationRecord
    #### Uploaders ####
    mount_uploader :image, ImageUploader
    
    #### Relations ####
    belongs_to :user
end
