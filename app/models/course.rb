class Course < ApplicationRecord
    #### Acts as Paranoid ####
    acts_as_paranoid

    #### Concerns ####
    include UuidProvider

    #### Relations ####
    has_many :course_units, dependent: :destroy
    has_many :course_sub_units, through: :course_units

    #### Callbacks ####
    # after_touch :update_details
    # before_update :update_details

    private

    # def update_details
        # self.modified_by = ""
    # end
end
