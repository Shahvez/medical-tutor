class Director < User

    #### Relations ####
    has_many :director_counsellors
    has_many :counsellors, through: :director_counsellors
    

    ### Scope ####
    default_scope { where(:user_type => 'director') }

end
