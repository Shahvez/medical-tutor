class ApplicationController < ActionController::Base
    add_flash_types :success, :info, :error, :warning

    def after_sign_in_path_for(resource)
        admin_homes_path
    end
end
