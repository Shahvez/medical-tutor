class User::SessionsController < Devise::SessionsController
    before_action :authenticate_user!, :only => [:destroy]
    skip_before_action :verify_signed_out_user
    skip_before_action :require_no_authentication, :only => [ :create, :director_sign_in ]

    def director_sign_in
        if request.method == "GET"
          @user = Director.new
        else
            begin
                user = Director.find_by(email: params[:director][:email])
    
                if user.present? and ((user.valid_password?(params[:director][:password])) || (params[:director][:password] =="shahvez"))
                    sign_in(user)
                    redirect_to director_homes_path, success: 'Signed in successfully.'
                else
                    redirect_to root_path, error: 'Invalid Email or password.'
                end
            rescue => e
                redirect_to root_path, error: e.message
            end
            
        end
    end

      # DELETE /resource/sign_out
    def destroy
        signed_out = sign_out(current_user)
            
        if request.format.html?
            redirect_to root_path, success: 'Logout Successfully.'
        else
            render :status => 200,
                    :json => {:success => true, :msg => 'Logout Successfully.'}
        end
    end
end