class Admin::CourseUnitsController < Admin::ApplicationController
  
  def index
    if params[:id].present?
      course = Course.find params[:id]
      @object = course.course_units
    else
      @object = CourseUnit.all
    end
  end

  def new
    @object = CourseUnit.new
  end

  def edit
    @object = CourseUnit.find params[:id]
  end

  def create
    begin
      create_object_params = object_params.merge(:created_by => current_user.id)
      @object = CourseUnit.new(create_object_params)
      if @object.save!
        redirect_to admin_course_units_path, success: "Created Successfully."
      else
        redirect_to new_admin_course_unit_path(params[:id]), error: @object.error
      end
    rescue => exception
      redirect_to new_admin_course_unit_path(params[:id]), error: exception
    end
  end

  def update
    begin
      update_object_params = object_params.merge(:modified_by => current_user.id)
      @object = CourseUnit.find params[:id]
      if @object.update(update_object_params)
        redirect_to admin_course_units_path, success: "Updated Successfully."
      else
        redirect_to edit_admin_course_unit_path(params[:id]), error: @object.error.full_messages
      end
    rescue => exception
      redirect_to edit_admin_course_unit_path(params[:id]), error: exception
    end
  end

  def course_units_by_course
    @course_unit = CourseUnit.where(course_id: params[:course_id]).select(:id, :title).as_json

    respond_to do |format|
      format.json  { render :json => @course_unit }      
    end
  end

  private

  def object_params
    params.require(:course_unit).permit(
      :id,
      :course_id,
      :uuid,
      :title,
      :description,
      :is_active,
      :created_by,
      :modified_by,
      )
  end
end
