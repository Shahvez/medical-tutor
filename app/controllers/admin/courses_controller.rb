class Admin::CoursesController < Admin::ApplicationController
  
  def index
    @object = Course.all
  end

  def new
    @object = Course.new
  end

  def edit
    @object = Course.find params[:id]
  end

  def create
    begin
      create_object_params = object_params.merge(:created_by => current_user.id)
      @object = Course.new(create_object_params)
      if @object.save!
        redirect_to admin_courses_path, success: "Created Successfully."
      else
        redirect_to new_admin_course_path(params[:id]), error: @object.error
      end
    rescue => exception
      redirect_to new_admin_course_path(params[:id]), error: exception
    end
  end

  def update
    begin
      update_object_params = object_params.merge(:modified_by => current_user.id)
      @object = Course.find params[:id]
      if @object.update(update_object_params)
        redirect_to admin_courses_path, success: "Updated Successfully."
      else
        redirect_to edit_admin_course_path(params[:id]), error: @object.error.full_messages
      end
    rescue => exception
      redirect_to edit_admin_course_path(params[:id]), error: exception
    end
  end

  private

  def object_params
    params.require(:course).permit(
      :uuid,
      :title,
      :description,
      :is_active,
      :created_by,
      :modified_by,
      )
  end
end
