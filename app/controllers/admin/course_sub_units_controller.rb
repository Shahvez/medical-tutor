class Admin::CourseSubUnitsController < Admin::ApplicationController
  
  def index
    if params[:id].present?
      course_unit = CourseUnit.find params[:id]
      @object = course_unit.course_sub_units
    else
      @object = CourseSubUnit.all
    end
  end

  def new
    @object = CourseSubUnit.new
    @course_units = []
  end

  def edit
    @object = CourseSubUnit.find params[:id]
    @course_units = CourseUnit.where(id: @object.course_unit)
  end

  def create
    begin
      create_object_params = object_params.merge(:created_by => current_user.id)
      @object = CourseSubUnit.new(create_object_params)
      if @object.save!
        redirect_to admin_course_sub_units_path, success: "Created Successfully."
      else
        redirect_to new_admin_course_sub_unit_path(params[:id]), error: @object.error
      end
    rescue => exception
      redirect_to new_admin_course_sub_unit_path(params[:id]), error: exception
    end
  end

  def update
    begin
      update_object_params = object_params.merge(:modified_by => current_user.id)
      @object = CourseSubUnit.find params[:id]
      if @object.update(update_object_params)
        redirect_to admin_course_sub_units_path, success: "Updated Successfully."
      else
        redirect_to edit_admin_course_sub_unit_path(params[:id]), error: @object.error.full_messages
      end
    rescue => exception
      redirect_to edit_admin_course_sub_unit_path(params[:id]), error: exception
    end
  end

  private

  def object_params
    params.require(:course_sub_unit).permit(
      :id,
      :course_unit_id,
      :uuid,
      :title,
      :description,
      :is_active,
      :created_by,
      :modified_by,
      )
  end
end
