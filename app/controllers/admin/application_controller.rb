class Admin::ApplicationController < ApplicationController
    layout "admin"
    before_action :authenticate_user!
    before_action :check_admin

    def check_admin
        if !current_user.admin?
            sign_out(current_user)
            redirect_to root_path, warning: "Unauthorized Access."
        end
    end
end
