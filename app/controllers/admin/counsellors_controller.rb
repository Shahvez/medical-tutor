class Admin::CounsellorsController < Admin::ApplicationController
  def index
    @user = Counsellor.all
  end

  def new
    @user = Counsellor.new
  end

  def edit
    @user = Counsellor.find params[:id]
  end

  def create
    begin
      @user = Counsellor.new(user_params)
      if @user.save!
        director_id = params[director_id].present? ? params[director_id] : current_user.id
        Counsellor.add_director_counsellor(director: director_id, counsellor: @user.id)
        redirect_to admin_counsellors_path, success: "Created Successfully."
      else
        redirect_to new_admin_counsellor_path(params[:id]), error: @user.error
      end
    rescue => exception
      redirect_to new_admin_counsellor_path(params[:id]), error: exception
    end
  end

  def update
    begin
      @user = Counsellor.find params[:id]
      if @user.update_columns(
        first_name: user_params[:first_name],
        last_name: user_params[:last_name],
        dob: user_params[:dob],
        gender: user_params[:gender],
        contact_number: user_params[:contact_number],
        user_type: user_params[:user_type],
        father_name: user_params[:father_name],
        mother_name: user_params[:mother_name],
        identity_number: user_params[:identity_number],
        street_address: user_params[:street_address],
        city: user_params[:city],
        state: user_params[:state],
        zip: user_params[:zip],
        is_active: user_params[:is_active],
        status: user_params[:status] || 0
        )

        redirect_to admin_counsellors_path, success: "Updated Successfully."
      else
        redirect_to edit_admin_counsellor_path(params[:id]), error: @user.error.full_messages
      end      
    rescue => exception
      redirect_to edit_admin_counsellor_path(params[:id]), error: exception
    end
  end

  private

  def user_params
    params.require(:counsellor).permit(
      :email,
      :password,
      :first_name,
      :last_name,
      :image,
      :dob,
      :gender,
      :contact_number,
      :user_type,
      :father_name,
      :mother_name,
      :identity_number,
      :street_address,
      :city,
      :state,
      :zip,
      :latitude,
      :longitude,
      :timezone,
      :is_active,
      :state
      )
  end
end
