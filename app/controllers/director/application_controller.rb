class Director::ApplicationController < ApplicationController
    layout "director"
    
    before_action :authenticate_user!
    before_action :check_director

    def check_director
        if !current_user.director?
            sign_out(current_user)
            redirect_to root_path, warning: "Unauthorized Access."
        end
    end
    
end
