class Director::HomesController < Director::ApplicationController
  
  def index
    @user = Director.where(id: current_user)
  end

  def new
  end

  def edit
  end
end
