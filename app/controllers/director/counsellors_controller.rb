class Director::CounsellorsController < Director::ApplicationController
  
  def index
    @user = Counsellor.joins(:director_counsellors).where("director_counsellors.director_id = ?", current_user)
  end

  def new
    @user = Counsellor.new
  end

  def edit
    @user = Counsellor.find params[:id]
  end

  def create
    begin
      @user = Counsellor.new(user_params)
      if @user.save!
        Counsellor.add_director_counsellor(director: current_user, counsellor: @user)
        redirect_to director_counsellors_path, success: "Created Successfully."
      else
        redirect_to new_director_counsellor_path(params[:id]), error: @user.error
      end
    rescue => exception
      redirect_to new_director_counsellor_path(params[:id]), error: exception
    end
  end

  def update
    begin
      @user = Counsellor.find params[:id]
      if @user.update(user_params)
        redirect_to director_counsellors_path, success: "Updated Successfully."
      else
        redirect_to edit_director_counsellor_path(params[:id]), error: @user.error.full_messages
      end
    rescue => exception
      redirect_to edit_director_counsellor_path(params[:id]), error: exception
    end
  end

  private

  def user_params
    params.require(:counsellor).permit(
      :email,
      :password,
      :first_name,
      :last_name,
      :image,
      :dob,
      :gender,
      :contact_number,
      :user_type,
      :father_name,
      :mother_name,
      :identity_number,
      :street_address,
      :city,
      :state,
      :zip,
      :latitude,
      :longitude,
      :timezone,
      :is_active,
      :state
      )
  end
end
